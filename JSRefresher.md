# JS Refresher
JavaScript is a scripting language that was invented to make web pages dynamic and more appealing for users. Because the internet as we know it is huge, there is an organization that is tasked with standardizing the tools that we use to build web pages. One of the standards is the ECMA standard, for standardizing the features for the JavaScript language. That is why most of the time JavaScript is referred to as ECMAScript. Basically, they are the same, but ECMAScript is the standardized version. These standards are set and browsers try and implement them as best as possible. That is why some features that are new take some time to be implemented in modern browsers and are missing from older ones. Even tho the name JavaScript has Java in it, it has nothing to do with Java. For more information on the ECMAScript standards as well as the history of the naming of this language, check the resources below for links. 

## Variables and DataTypes
Unlike languages such as C# and Java, JavaScript is a loosely typed and dynamic language. We can declare variables without knowing the data type that the variable will hold in it. With that said, we can't specify what type is a certain variable, but this does not mean that data types do not exist in JavaScript. This just means that the assigning of a type is dynamic. Dynamic assigning of a type works by JavaScript first **evaluating the expression from left to right** and then detecting the data type of the result of that expression. That data type is then assigned to the variable along with the value. If we try and assign another expression to that variable, it will go through the same process and change the data type if needed. Variables can be declared by writing the **var or let** keyword. Both of them mean the same thing but have different rules that they follow ( More on that in later sessions ). If a variable is empty, then it has value and data type as **undefined**. 
```js
var variable1 = 2; // DataType is number, value is 2
var variable2 = "Hello"; // DataType is string, value is "Hello"
var variable3 = true; // DataType is boolean
var variable4 = 2 + 5; // DataType is number, value is 7
var variable5 = 2 + 5 + "Hello"; // DataType is string, value  is 7Hello
var variable6 = "Hello" + 2 + 5; // DataType is string, value  is Hello25
var variable7; // DataType is undefined, value is undefined

variable7 = 5; // The DataType changed to number, value changed to 5
variable1 = "Bye"; // The DataType changed to string, value changed to "Bye"

var c, d, e; // Declaring multiple empty variables
c = d = e = 10; // Setting multiple variables to one value
var x = 5, y = 6, z = 7; // Declaring multiple different value variables

var typeofx = typeof(x); // Returns the type of a variable as a string
```
## The console
The console of the browser is a very helpful tool when working with JavaScript. We can manipulate it and use its features through the **console** object.  The console is read-only and can't request for input from a user, only show data at a particular time of the execution. We can, however, run JavaScript code directly in the console for testing and checking purposes. That code will be executed after the main script has finished and will have access to all functions and variables accessible from the main script. Usually, we use the console to log information during the execution of our script, to check a value, a type, or the state of an object. 
```js
// log
console.log("Hello!"); // Logs an entry in the console
// Outcome: Hello
var num = 5;
console.log(num); // Logs an entry from a variable in the console
// Outcome: 5
console.log("These are two different values: ", num, " and ", 7); // Logs multiple values separately in a single log
// Outcome: These are two different values:  5  and  7
console.log({name: "Will", phone: 38970555444}); // Logs an object in to the console
// Outcome: > Object { name: "Will", phone: 38970555444 }

// Different types of logs ( Can be enabled, disabled from the browser )
console.info("Hey!"); // For showing extra information log
console.warn("Hey!"); // For showing a warning log ( Yellow on most browsers )
console.error("Hey!"); // For showing an error log ( Red on most browsers )
console.debug("Hey!"); // For showing a developers only log ( Turned off by default )

// Track time 
console.time();
// ... Some code 
// ... Some code
console.timeEnd();
// Outcome: 7240.431884765625 ms

// Group logs
console.log("App logs:"); // Not grouped
console.group("Login logs"); // Grouped logs
console.log("In progress...");
console.log("Authenticating...");
console.log("Done!");
console.groupEnd()
console.log("App finished!"); // Not grouped
// Outcome:
// App logs:
// > Login logs
//     In progress...  
//     Authenticating...
//     Done!
// App finished!

// Stack Trace logs
```js
function first() {
  console.log("First starts...");
  second();
}
function second() {
  console.log("Second starts...");
  third();
}
function third() {
  console.log("Third starts...");
  console.trace();
}
first();
// Outcome:
First starts...
Second, starts...
Third starts...
> console.trace
    third @ VM1468:11
    second @ VM1468:7
    first @ VM1468:3
    (anonymous) @ VM1468:13


// Table logs
var people = [{
	name: "Will",
	phone: 38970555444
},
{
	name: "May",
	phone: 38975384222
}];
console.table(people);
// Outcome: Table with index, name and phone as columns and the data as rows

// Styled logs
// %c represents color
console.log("%c Text in a color.", "color:green"); // Log with text in different color
console.log("%c Welcome to the Matrix", "color:green; background-color:black"); // Log with text and background in different color

// Clear console
console.clear();
```

## Loops and Arrays
In JavaScript, like in most languages, we store multiple values in an Array. Since the language is loosely typed and dynamic, Arrays do not need to be declared with a separate keyword, can accept any type of value, can accept different types of values, and do not have a strict limit or specific declaration on the length of the array. There are methods for manipulating arrays as well as loops for going through them. 

#### Arrays
```js
// Declaration
var emptyArray = []; // Declaring an empty array
var numbersArray = [2,5,7,9,100]; // Declaring an array with numbers
var randomArray = [true, "Bill", 233, null]; // Declaring an array with different types
console.log(numbersArray.length); // Returns the number of items in the array
// Outcome: 5
console.log(randomArray[1]); // Accessing an item in the array
// Outcome: "Bill"
var billIndex = randomArray.indexOf("Bill");
// Outcome: billIndex = 1

// Methods
// Adding/Removing items in an array
numbersArray.push(40); // Add item to the back of the array
// Outcome: [2,5,7,9,100,40]
numbersArray.unshift(35); // Add item to the front of the array
// Outcome: [35,2,5,7,9,100,40]
numbersArray.pop(); // Remove item from the back of the array
// Outcome: [35,2,5,7,9,100]
numbersArray.shift(); // Add item from the front of the array
// Outcome: [2,5,7,9,100]
var removed = numbersArray.splice(1, 3); // Removes a set of items from an array between given indexes ( including those indexes )
// Outcome: 
// removed = [5,7,9]
// numbersArray = [2,100]

// Array manipulation
var someArray = [12,13,14,15]
var numbersString = someArray.join("-"); // Creates a string of the items in the array with a given separator ( , is default )
// Outcome: "12-13-14-15"
var slicedNumbers = someArray.slice(1,3); // Slices the numbers between the indexes, not including the end index it self. It does not change the original array
// Outcome:
// slicedNumbers = [13, 14]
// someArray = [12, 13, 14, 15]
someArray.reverse(); // reverses the items in reverse order
// Outcome: someArray = [15, 14, 13, 12]
```
#### Loops
```js
var numbersArray = [235, undefined ,266,127, 199, null, 100, 603, 123];
var winningNumber = 603;
// For
console.log("Your numbers:");
for(i = 0; i < numbersArray.length; i++){
	if(typeof(numbersArray[i]) != "number"){
		continue; // Skips this cycle and continues with the rest
    }
    console.log(numbersArray[i]);
	if(numbersArray[i] == winningNumber){
		console.log("YOU WON!");
		break; // Stops the loop even if it has more cycles to go
	}
}

// While
var input;
while(input.toLowerCase() != "x" ){
	input = prompt("Enter x to continue!");
}
console.log("Welcome!");

// For-of
var names = ["Bill","Ted","Tom","Susan"];
for(var name of names){
	console.log("Welcome " + name);
}
```