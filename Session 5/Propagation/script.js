// Selectors
let list = document.getElementById("list");

// Event listeners
list.addEventListener("click", function(e){
    console.log("ul");
})
list.firstElementChild.addEventListener("click", function(e){
    console.log("li");
    // e.stopPropagation();
    // e.stopImmediatePropagation();
})
list.firstElementChild.firstElementChild.addEventListener("click", function(e){
    console.log("a 1");
    // e.stopPropagation();
    // e.stopImmediatePropagation();
    // e.preventDefault();
})
list.firstElementChild.firstElementChild.addEventListener("click", function(e){
    console.log("a 2");
})