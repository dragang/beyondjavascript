// Selectors
let list = document.getElementById("list");
let btn = document.getElementById("demoBtn");
let input = document.getElementById("demoInput");

// Event listeners
// Click
btn.addEventListener("click", function(event){
    console.log(event);
})
// Key
input.addEventListener("keyup", function(e){
    console.log(e);
})
// Target Elements
list.addEventListener("click", function(e){
    console.log("Target:");
    console.log(e.target);
    console.log("Current Target:");
    console.log(e.currentTarget);
    console.log("This:");
    console.log(this);
})