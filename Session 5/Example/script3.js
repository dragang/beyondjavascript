let main = document.getElementById("main");
let list = main.getElementsByTagName("ul")[0];
let listItems = list.children;

// Separate logic in functions
function editItem(event){
    let input = event.currentTarget.children[1];
    let grocery = event.currentTarget.children[0];
    input.value = grocery.innerText;
    input.classList.remove("hidden");
    grocery.classList.add("hidden");
    input.focus();
}
function finishEditing(event){
    let input = event.currentTarget;
    let grocery = event.currentTarget.previousElementSibling;
    grocery.innerText = input.value;
    grocery.classList.remove("hidden");
    input.classList.add("hidden");
}
// Using for to loop through all elements and add handler to every one of them
for (let item of listItems) {
    item.addEventListener("click", editItem);
    item.children[1].addEventListener("blur", finishEditing);
}