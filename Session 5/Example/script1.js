let main = document.getElementById("main");
let list = main.getElementsByTagName("ul")[0];
let listItems = list.children;

let firstInput = document.getElementsByTagName("input")[0];
let firstSpan = document.getElementsByTagName("span")[0];
let secondInput = document.getElementsByTagName("input")[1];
let secondSpan = document.getElementsByTagName("span")[1];
let thirdInput = document.getElementsByTagName("input")[2];
let thirdSpan = document.getElementsByTagName("span")[2];

listItems[0].addEventListener("click", function(){
    firstInput.value = firstSpan.innerText;
    firstInput.classList.remove("hidden");
    firstSpan.classList.add("hidden");
    firstInput.focus();
})

firstInput.addEventListener("blur", function(){
    firstSpan.innerText = firstInput.value;
    firstSpan.classList.remove("hidden");
    firstInput.classList.add("hidden");
})

listItems[1].addEventListener("click", function(){
    secondInput.value = secondSpan.innerText;
    secondInput.classList.remove("hidden");
    secondSpan.classList.add("hidden");
    secondInput.focus();
})

secondInput.addEventListener("blur", function(){
    secondSpan.innerText = secondInput.value;
    secondSpan.classList.remove("hidden");
    secondInput.classList.add("hidden");
})

listItems[2].addEventListener("click", function(){
    thirdInput.value = thirdSpan.innerText;
    thirdInput.classList.remove("hidden");
    thirdSpan.classList.add("hidden");
    thirdInput.focus();
})

thirdInput.addEventListener("blur", function(){
    thirdSpan.innerText = thirdInput.value;
    thirdSpan.classList.remove("hidden");
    thirdInput.classList.add("hidden");
})