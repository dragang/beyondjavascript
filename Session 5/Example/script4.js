// Having an array of items
let main = document.getElementById("main");
let list = main.getElementsByTagName("ul")[0];
let btnGroup = document.getElementById("btnGroup");
let items = ["Potato", "Cheese", "Bacon"];

// Function to generate a grocery item
function generateGroceryItem(value){
    let newItem = document.createElement("li");
    let grocerySpan = document.createElement("span");
    grocerySpan.innerText = value;
    let groceryInput = document.createElement("input");
    groceryInput.classList.add("hidden");
    newItem.appendChild(grocerySpan);
    newItem.appendChild(groceryInput);
    groceryInput.addEventListener("blur", finishEditing);
    return newItem;
}
// Separate logic in functions
function editItem(event){
    let item = event.target.parentElement;
    let input = item.children[1];
    let grocery = item.children[0];
    input.value = grocery.innerText;
    input.classList.remove("hidden");
    grocery.classList.add("hidden");
    input.focus();
}
function finishEditing(event){
    console.log(event.currentTarget);
    let input = event.currentTarget;
    let grocery = event.currentTarget.previousElementSibling;
    grocery.innerText = input.value;
    grocery.classList.remove("hidden");
    input.classList.add("hidden");
}

// Create all HTML list items from the array items
for(let i = 0; i < items.length; i++){
    list.append(generateGroceryItem(items[i]));
}

// Listen on the list with one event listener for every click instead of appending event listener to all created List Items
list.addEventListener("click", function(e){
    if(e.target.nodeName == "SPAN"){
        editItem(e);
    }
});

// Another example of using one event listener for multiple elements
btnGroup.addEventListener("click", function(e){
    let result = e.target.getAttribute("data-result");
    if(result == "Cancel") console.log("Action Canceled!");
    if(result == "Save") console.log("Groceries Saved!");
});

