let main = document.getElementById("main");
let list = main.getElementsByTagName("ul")[0];
let listItems = list.children;

// Using for loop to shorten process
for (let item of listItems) {
    let input = item.children[1];
    let grocery = item.children[0];

    item.addEventListener("click", function(){
        input.value = grocery.innerText;
        input.classList.remove("hidden");
        grocery.classList.add("hidden");
        input.focus();
    })

    input.addEventListener("blur", function(){
        grocery.innerText = input.value;
        grocery.classList.remove("hidden");
        input.classList.add("hidden");
    })
}