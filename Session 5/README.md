# JS Events 
Dynamic web pages work by detecting and using the interaction of the user. All of these interactions like clicking, typing on the keyboard, scrolling, hovering, etc. can be detected and we can write specific code to be executed at that particular moment. These moments are called events. Everything that we do in an HTML page can be detected and captured as an event. But JavaScript does not record or execute every single event. It only activates if there is code that listens for a particular event. Only events that are listened for can be detected and can affect the page.
## Event handlers
If we want to listen for a particular event, we need to implement an event handler. Event handlers are functions that require the type of event that we are waiting for, the element on which we are waiting for that event to happen, and a function that will execute when the event is raised. After we give the required values, we can execute the handler and wait for the user to do raise that particular event. If we wait for a click to happen to a button when the user clicks on that particular button, an event will be raised and the code we gave will execute. Events are also captured and can be used in a handler to check what the state of the page was when the event happened. It encapsulates the moment when the event happened with a lot of data such as the precise coordinates of the mouse, wether some button was held, like shift or ctrl, and a lot more. There are 3 types of event handlers:
* HTML Inline handler
* JavaScript Inline handler
* Event Listener

#### HTML Page and HTML Inline handler
```html
<h1>Handlers</h1>
    <div id="main">
	    <h3>List of items for target example</h3>
        <ul id="list">
            <li><a>Potato</a></li>
            <li><a>Cheese</a></li>
            <li><a>Bacon</a></li>
        </ul>
        <h3>Different handlers and click events</h3>
        <button id="demoBtn1">Click me inline from js</button>
        <button ondblclick="hello()">Click me inline from html</button>
        <button id="demoBtn2">Click me from listener</button>
        <h3>Keyboard events</h3>
        <input id="up" type="text" placeholder="up">
        <input id="down" type="text" placeholder="down">
        <input id="press" type="text" placeholder="press">
        <h3>Load events</h3>
        <img id="largeImage" src="http://www.effigis.com/wp-content/uploads/2015/02/Infoterra_Terrasar-X_1_75_m_Radar_2007DEC15_Toronto_EEC-RE_8bits_sub_r_12.jpg" alt="big image">
    </div>
```
```js
// Selectors
let btn1 = document.getElementById("demoBtn1");
let btn2 = document.getElementById("demoBtn2");
let up = document.getElementById("up");
let down = document.getElementById("down");
let press = document.getElementById("press");
let img = document.getElementById("largeImage");

// Functions
function hello(person){
    console.log("Hello!");
}
function bye(){
    console.log("Bye!");
}

// Handlers
// Inline handler with fucntion reference
btn1.onclick = hello;
// Inline handler with annonimous function
btn1.onclick = function() {
    console.log("Bye!");
}
// Event listener with function reference
btn2.addEventListener("click", hello);
btn2.addEventListener("click", function(){
    console.log("Bye!");
})

// Keyboard events
up.addEventListener("keyup", function(){
    console.log("Key up pressed");
});
down.addEventListener("keydown", function(){
    console.log("Key down pressed");
});
press.addEventListener("keypress", function(){
    console.log("Key press pressed");
});

// Load events
window.addEventListener("load", function(){
    console.log("window is loaded!");
})
document.addEventListener("DOMContentLoaded", function() {
    console.log("DOM fully loaded");
});
img.addEventListener("load", function(){
   console.log("image is loaded!");
});

// Capture and Target Elements
list.addEventListener("click", function(e){
    console.log("Target:");
    console.log(e.target);
    console.log("Current Target:");
    console.log(e.currentTarget);
    console.log("This:");
    console.log(this);
})
```
## Propagation
Propagation is a term that describes the way events are handled on overlapping elements. This happens when we have 3 elements one inside of another and on which we have events. Propagation decides the order in which the events will execute when we trigger the event on the innermost element. By triggering that event the rest of the events on the parent elements are also triggered and with propagation, the order is determined. There are two ways we can do propagation:
 * Bubbling - Events are propagated from the innermost element to the outside
 * Capturing - Events are propagated from the outermost element to the inside

We can also stop propagation at will if necessary or even stop default browser behavior. We can do that with a few functions:
| Method                     | Stop propagation on parents | Stop other events | Stop browser default action |
|----------------------------|-----------------------------|-------------------|-----------------------------|
| stopPropagation()          | ✓                           | ✕                 | ✕                           |
| stopImmediatePropagation() | ✓                           | ✓                 | ✕                           |
| preventDefault()           | ✕                           | ✕                 | ✓                           |

#### Example of propagation
```html
<div id="main">
    <h2>Propagation explained:</h2>
    <ul id="list">
        <li><a href="https://google.com/">Potato</a></li>
        <li><a>Cheese</a></li>
        <li><a>Bacon</a></li>
    </ul>
</div>
```
```js
// Selectors
let list = document.getElementById("list");

// Event listeners
list.addEventListener("click", function(e){
    console.log("ul");
})
list.firstElementChild.addEventListener("click", function(e){
    console.log("li");
    // e.stopPropagation();
    // e.stopImmediatePropagation();
})
list.firstElementChild.firstElementChild.addEventListener("click", function(e){
    console.log("a 1");
    // e.stopPropagation();
    // e.stopImmediatePropagation();
    // e.preventDefault();
})
list.firstElementChild.firstElementChild.addEventListener("click", function(e){
    console.log("a 2");
})
```