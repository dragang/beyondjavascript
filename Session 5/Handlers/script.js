// Selectors
let btn1 = document.getElementById("demoBtn1");
let btn2 = document.getElementById("demoBtn2");
let up = document.getElementById("up");
let down = document.getElementById("down");
let press = document.getElementById("press");
let img = document.getElementById("largeImage");

// Functions
function hello(person){
    console.log("Hello!");
}
function bye(){
    console.log("Bye!");
}

// Handlers
// Inline handler with fucntion reference
btn1.onclick = hello;
// Inline handler with annonimous function
btn1.onclick = function() {
    console.log("Bye!");
}
// Event listener with function reference
btn2.addEventListener("click", hello);
btn2.addEventListener("click", function(){
    console.log("Bye!");
})

// Keyboard events
up.addEventListener("keyup", function(){
    console.log("Key up pressed");
});
down.addEventListener("keydown", function(){
    console.log("Key down pressed");
});
press.addEventListener("keypress", function(){
    console.log("Key press pressed");
});

// Load events
window.addEventListener("load", function(){
    console.log("window is loaded!");
})
document.addEventListener("DOMContentLoaded", function() {
    console.log("DOM fully loaded");
  });
img.addEventListener("load", function(){
   console.log("image is loaded!");
});

