// Inheritance with classes
class Person {
    constructor(first, last, age){ // constructor where we set values from the outside world
        this.firstName = first;
        this.lastName = last;
        this.age = age;
    }
    getFullName(){ // methods dont need 'this' in front of them
        return `${this.firstName} ${this.lastName}`; // inside of methods we still need this to refference the current object
    }
}
let somePerson = new Person("Bob", "Bobsky", 44);
console.log(somePerson);
console.log(somePerson.getFullName());

class User extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
}
let admin = new User("Gosha", "admin", "Mr", "Kirov", 25);
console.log(admin);
console.log(admin.getFullName());
console.log(`Can edit: ${admin.canEdit()}`);

// // Static methods in classes
class User2 extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
    static giveAdminRights(user){ // static function that is set on the User2 class, not on the object created
        if(user.role !== "admin"){
            user.role = "admin";
        } else {
            console.log("The is already admin!");
        }
    }
}

let greg = new User2("user007", "user", "Greg", "Gregory", 30);
console.log(greg);
User2.giveAdminRights(greg);
//greg.giveAdminRights(greg); // will throw an error
console.log(greg);

// // Get/Set in classes in JavaScript
// Static methods in classes
class User3 extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    get userName(){ // getter for userName property
        return this._userName; // underscore fields are already assigned in the background
    }
    set userName(value){ // setter for userName property
        value.length > 4 ? this._userName = value : (function (){throw new Error("Name is too short")})();
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
}

let peter = new User3("peter29", "user", "Peter", "Peterson", 29);
console.log(peter.userName);
let jo = new User3("jo", "user", "Jo", "Swanson", 54);
console.log(jo.userName);