// Creating objects
// Object literal
let someObj1 = { 
    type: "obj",
    whatsThis: function(){ console.log(`This is: ${this.type}`); }
};
someObj1.whatsThis();
// Constructor Function
let someObj2 = new Object();
someObj2.type = "obj";
someObj2.whatsThis = function(){ console.log(`This is: ${this.type}`); }

// They are the same
console.log(someObj1);
console.log(someObj2);
someObj1.whatsThis();
someObj2.whatsThis();
console.log(Object.getPrototypeOf(someObj1));
console.log(Object.getPrototypeOf(someObj2));

// Object methods
let name = { firstName: "Bob", age: 24 };
let last = { lastName: "Bobsky", age: 28 };
let bob = Object.assign(name, last); // Merges two objects together
// console.log(bob);

console.log(Object.keys(bob)); // gets keys from object
console.log(Object.values(bob)); // gets values from object
console.log(Object.entries(bob)); // gets key/value pairs from object !! ES8 !!

Object.seal(name); // Disables the creation of new entities
name.newProperty = "Stuff";
name.firstName = "Jill";
console.log(name.newProperty);
console.log(name.firstName);

Object.freeze(last); // Disables the creation of new entities and changing of the existing ones
last.newProperty = "Stuff";
last.lastName = "Jilette";
console.log(last.newProperty);
console.log(last.lastName);