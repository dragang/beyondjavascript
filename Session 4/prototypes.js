// Constructor function ( acting as class )
function Person(first, last, age){
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.getFullName = function(){
        return `${this.firstName} ${this.lastName}`;
    }
}
let somePerson = new Person("Bob", "Bobsky", 44);
console.log(somePerson);
console.log(somePerson.getFullName());

// without new
function Person1(first, last, age){
    return {
        firstName: first,
        lastName: last,
        age: age,
        getFullName: function(){
            return `${this.firstName} ${this.lastName}`;
        }
    }
}
let somePerson1 = new Person1("Bob", "Bobsky", 44);
// Inheritance 
// Not so convinient way of inheritance
let personKirov = new Person("Mr", "Kirov", 25);
let adminUser = Object.create(personKirov); // create method creates a new object with the attribute as a prototype
adminUser.userName = "Gosha";
adminUser.role = "admin";
adminUser.canEdit = function(){
    return this.role === "admin" ? true : false;
}
adminUser.getFullName = function(){
    return "HELLO TONKA!";
}
adminUser.__proto__ = Object.create(personKirov);

console.log(adminUser);
console.log(adminUser.getFullName());
console.log(adminUser.__proto__.getFullName());
console.log(`Can edit: ${adminUser.canEdit()}`);


// Inheritance done better with prototypes
function User(username, role, first, last, age){
    // this.__proto__ = new Person(first, last, age); // Bad practice
    Object.setPrototypeOf(this, new Person(first, last, age)); // Inheriting from Person
    this.userName = username;
    this.role = role;
    this.canEdit = function(){
        return this.role === "admin" ? true : false;
    }
}
let admin = new User("Gosha", "admin", "Mr", "Kirov", 25);
console.log(admin);

console.log(Object.getPrototypeOf(admin));

// Adding things to an object prototype ( not recommended )
Person.prototype.walk = function(){
    console.log(`The person ${this.firstName} is walking!`);
}

somePerson.walk();
admin.walk();