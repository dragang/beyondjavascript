# JS Object-Oriented
JavaScript was not designed to support modern object-oriented programming. Because of this, there had to be a lot of unique ways that developers can practice this sort of paradigm. If you are thinking of writing object-oriented programming in javascript few things are unique to the language and that you need to keep in mind:
* Is prototype-based - Uses prototype-based inheritance instead of class-based
* Everything is an object - Treats every entity as an object
* Mimics classes as functions - Can't have classes, just functions that are made to act like classes
* Have to make our implementation for private fields - There is no core implementation for private fields 

Most of the time, developers use objects in JavaScript as anonymous objects. They can be created on the fly and can have any properties and methods, dynamicly change the values and the types as well. 
```js
// Creating objects
// Object literal
let someObj1 = { 
    type: "obj",
    whatsThis: function(){ console.log(`This is: ${this.type}`); }
};
someObj1.whatsThis();
// Constructor Function
let someObj2 = new Object();
someObj2.type = "obj";
someObj2.whatsThis = function(){ console.log(`This is: ${this.type}`); }

// They are the same
console.log(someObj1);
console.log(someObj2);
someObj1.whatsThis();
someObj2.whatsThis();
console.log(Object.getPrototypeOf(someObj1));
console.log(Object.getPrototypeOf(someObj2));

// Object methods
let name = { firstName: "Bob", age: 24 };
let last = { lastName: "Bobsky", age: 28 };
let bob = Object.assign(name, last); // Merges two objects together
// console.log(bob);

console.log(Object.keys(bob)); // gets keys from object
console.log(Object.values(bob)); // gets values from object
console.log(Object.entries(bob)); // gets key/value pairs from object !! ES8 !!

Object.seal(name); // Disables the creation of new entities
name.newProperty = "Stuff";
name.firstName = "Jill";
console.log(name.newProperty);
console.log(name.firstName);

Object.freeze(last); // Disables the creation of new entities and changing of the existing ones
last.newProperty = "Stuff";
last.lastName = "Jilette";
console.log(last.newProperty);
console.log(last.lastName);
```
## Prototype-based inheritance
Unlike popular object-oriented languages that use class-based inheritance, JavaScript uses a prototype-based inheritance. This means that instead of a class template inheriting from another class template, there is an object that inherits from another object and that parent object is called a prototype. This means that instead of using inheritance as an abstract chain of rules, we directly inherit from an existing object. There are also some similarities tho. For example, we can create chains of prototypes to create a logic tree just like other languages create a chain of classes to represent a logic tree. And even if classes do not exist classically, they exist as special functions that act like classes. Functions that act like classes do their jobs pretty well, to function as a class, and to even look like one in the new versions of the language. 

## Constructor functions
Functions that are acting like classes were used and they were called constructor functions. With these functions, we can create objects with a certain template. The naming convention is even the same, naming constructor functions with PascalCase and giving the name of the business logic entity such as Human or User. 

```js
// Constructor function ( acting as class )
function Person(first, last, age){
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.getFullName = function(){
        return `${this.firstName} ${this.lastName}`;
    }
}
let somePerson = new Person("Bob", "Bobsky", 44);
console.log(somePerson);
console.log(somePerson.getFullName());

// without new
function Person1(first, last, age){
    return {
        firstName: first,
        lastName: last,
        age: age,
        getFullName: function(){
            return `${this.firstName} ${this.lastName}`;
        }
    }
}
let somePerson1 = new Person1("Bob", "Bobsky", 44);
// Inheritance 
// Not so convinient way of inheritance
let personKirov = new Person("Mr", "Kirov", 25);
let adminUser = Object.create(personKirov); // create method creates a new object with the attribute as a prototype
adminUser.userName = "Gosha";
adminUser.role = "admin";
adminUser.canEdit = function(){
    return this.role === "admin" ? true : false;
}
adminUser.getFullName = function(){
    return "HELLO TONKA!";
}
adminUser.__proto__ = Object.create(personKirov);

console.log(adminUser);
console.log(adminUser.getFullName());
console.log(adminUser.__proto__.getFullName());
console.log(`Can edit: ${adminUser.canEdit()}`);


// Inheritance done better with prototypes
function User(username, role, first, last, age){
    // this.__proto__ = new Person(first, last, age); // Bad practice
    Object.setPrototypeOf(this, new Person(first, last, age)); // Inheriting from Person
    this.userName = username;
    this.role = role;
    this.canEdit = function(){
        return this.role === "admin" ? true : false;
    }
}
let admin = new User("Gosha", "admin", "Mr", "Kirov", 25);
console.log(admin);

console.log(Object.getPrototypeOf(admin));

// Adding things to an object prototype ( not recommended )
Person.prototype.walk = function(){
    console.log(`The person ${this.firstName} is walking!`);
}

somePerson.walk();
admin.walk();
```
## ES6 Classes
New versions of javascript evolved by wrapping special constructor functions in toa built-in keyword class and creating a more class-based object-oriented feel. These classes act a bit differently than a plain constructor function but are very easy to use and intuitive to write. They also create an option to manipulate with the constructor and parent constructor of the "class". Inheritance is also simplified a lot. 
```js
// Inheritance with classes
class Person {
    constructor(first, last, age){ // constructor where we set values from the outside world
        this.firstName = first;
        this.lastName = last;
        this.age = age;
    }
    getFullName(){ // methods dont need 'this' in front of them
        return `${this.firstName} ${this.lastName}`; // inside of methods we still need this to refference the current object
    }
}
let somePerson = new Person("Bob", "Bobsky", 44);
console.log(somePerson);
console.log(somePerson.getFullName());

class User extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
}
let admin = new User("Gosha", "admin", "Mr", "Kirov", 25);
console.log(admin);
console.log(admin.getFullName());
console.log(`Can edit: ${admin.canEdit()}`);

// // Static methods in classes
class User2 extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
    static giveAdminRights(user){ // static function that is set on the User2 class, not on the object created
        if(user.role !== "admin"){
            user.role = "admin";
        } else {
            console.log("The is already admin!");
        }
    }
}

let greg = new User2("user007", "user", "Greg", "Gregory", 30);
console.log(greg);
User2.giveAdminRights(greg);
//greg.giveAdminRights(greg); // will throw an error
console.log(greg);

// // Get/Set in classes in JavaScript
// Static methods in classes
class User3 extends Person{ // extends used for inheritance
    constructor(username, role, first, last, age){
        super(first, last, age); // accessing the parent constructor
        this.userName = username;
        this.role = role;
    }
    get userName(){ // getter for userName property
        return this._userName; // underscore fields are already assigned in the background
    }
    set userName(value){ // setter for userName property
        value.length > 4 ? this._userName = value : (function (){throw new Error("Name is too short")})();
    }
    canEdit(){
        return this.role === "admin" ? true : false;
    }
}

let peter = new User3("peter29", "user", "Peter", "Peterson", 29);
console.log(peter.userName);
let jo = new User3("jo", "user", "Jo", "Swanson", 54);
console.log(jo.userName);
```