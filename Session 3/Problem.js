// Our server holding documents
const serverDocuments = [
{id: 1, name:"Important Doc", size: 12.64},
{id: 4, name:"Daily Meeting Summary", size: 5.23},
{id: 4, name:"Specification_final_final", size: 8.5}
];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document){
    setTimeout(()=> {
        serverDocuments.push(document);
    }, 1000);
}

// The problem
addDocumentOnServer({id: 999, name: "New Document", size: 0.92});
getDocuments();