// Our server holding documents
const serverDocuments = [
    {id: 1, name:"Important Doc", size: 12.64},
    {id: 4, name:"Daily Meeting Summary", size: 5.23},
    {id: 4, name:"Specification_final_final", size: 8.5}
    ];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document){
    return new Promise((resolve, reject) => {
        setTimeout(()=> {
            serverDocuments.push(document);

            const error = document.name.length > 0 ? false : true;

            if(!error){
                resolve();
            } else {
                reject("Problem adding document on server!");
            }
        }, 1000);
    });
}
    
// The Promise Solution
addDocumentOnServer({id: 999, name: "", size: 0.92})
    .then(getDocuments)
    .catch(error => console.log(error))
    
