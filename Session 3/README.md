# JS Asynchronicity
As we know, JavaScript is executed in one thread. For us to do tasks that take time, we use the browser APIs to wait for us. But this creates a whole different problem. How do we know when we are going to get our data? How do we know when the browser will finish waiting so we can execute some code at that precise moment? This can bring issues like:
* Getting data from a server
* Getting data from hard disc
* Scheduling tasks for later
* Workaround blocking tasks

Asynchronous execution usually gets understood as running things in parallel or running multiple things at once, but in JavaScript it means, waiting on multiple things at the same time. 

To get around the issues caused by asynchronous programming in JavaScript we can use one of 3 options:
* Callback functions
* Promises
* Async/Await
#### Issue
Here we have two functions. One that adds a document to an imaginary server and one that reads the documents. When we execute both, we don't get all the documents with the new document added. We only get the old documents. We have to execute getDocuments() again manually to see that our new document was saved. This is a problem because while we are waiting for the document to be added on the server, the getDocuments() methods are executed and return only 3 documents. After a short while, the document will be saved, but it will be too late, since the getDocuments() function was already executed.
```js
// Our server holding documents
const serverDocuments = [
{id: 1, name:"Important Doc", size: 12.64},
{id: 4, name:"Daily Meeting Summary", size: 5.23},
{id: 4, name:"Specification_final_final", size: 8.5}
];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document){
    setTimeout(()=> {
        serverDocuments.push(document);
    }, 1000);
}

// The problem
addDocumentOnServer({id: 999, name: "New Document", size: 0.92});
getDocuments();
```
## Callback functions 
Callback functions are functions that are passed as a parameter and are executed when the waiting inside of the said function is done. If we have a chain of things that we need to wait for one by one to get a final result, we nest our callback functions one inside the other until we get to the last result. This works, but it creates an unreadable piece of code that looks like a huge triangle or pyramid usually referred to as:
* Callback Hell
* Pyramid of doom
* Hadouken programming

Before ES6 there were no other alternatives, so this was the only solution available at that time. Luckily there are new and better solutions these days.

#### Solution to the issue with callbacks
We send the getDocuments() method as a parameter to the addDocument and we make sure that in the addDocument that it is executed AFTER the document is saved. 
```js
// Our server holding documents
const serverDocuments = [
    {id: 1, name:"Important Doc", size: 12.64},
    {id: 4, name:"Daily Meeting Summary", size: 5.23},
    {id: 4, name:"Specification_final_final", size: 8.5}
    ];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document, callback){
    setTimeout(()=> {
        serverDocuments.push(document);
        callback();
    }, 1000);
}
    
// The callback Solution
addDocumentOnServer({id: 999, name: "New Document", size: 0.92}, getDocuments);
```
## Promises
Promises are a great way of handling waiting tasks. They are objects that have a state and can wait out a task while it finishes. As the name suggests, when it is returned from a function we get a promise that there will be an answer at some point in the future. The promise at this point also has a corresponding status, stating that the promise is still not resolved. When we get the response or result of the waiting task, we either get a resolved promise, with the data needed or a rejected promise, meaning there was an issue with getting the data or result.  Promises are standardized in ES6 and can be used in most modern browsers. 

#### Solution to the issue with promises
Here, we return a promise from the addDocument function. Then we can wait for the promise to get resolved outside of the function. When it gets resolved we execute the getDocuments function.
```js
// Our server holding documents
const serverDocuments = [
    {id: 1, name:"Important Doc", size: 12.64},
    {id: 4, name:"Daily Meeting Summary", size: 5.23},
    {id: 4, name:"Specification_final_final", size: 8.5}
    ];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document){
    return new Promise((resolve, reject) => {
        setTimeout(()=> {
            serverDocuments.push(document);

            const error = document.name.length > 0 ? false : true;

            if(!error){
                resolve();
            } else {
                reject("Problem adding document on server!");
            }
        }, 1000);
    });
}
    
// The Promise Solution
addDocumentOnServer({id: 999, name: "", size: 0.92})
    .then(getDocuments)
    .catch(error => console.log(error))
```
## Async/Await
Async/Await is a fancy way of handling promises without the extra methods and code.  It works by creating an async function. Inside the async function, we can handle all promises by just typing await in front of it. This will automatically wait until the promise is resolved and then return the result. If there is no await, the function will not wait for the answer to that promise and will continue. Await can't be used outside of an async function. 

#### Solution to the issue with async/await
We return a promise from the addDocument function here and then we handle both addDocument and getDocuments functions in an async function and we await for the addDocument to finish.
```js
// Our server holding documents
const serverDocuments = [
    {id: 1, name:"Important Doc", size: 12.64},
    {id: 4, name:"Daily Meeting Summary", size: 5.23},
    {id: 4, name:"Specification_final_final", size: 8.5}
    ];
// Function that gets all documents from server
function getDocuments(){
    setTimeout(()=> {
        for (const document of serverDocuments) {
            console.log(document.name);
        }
    }, 200);
}
// Function that adds a document to server
function addDocumentOnServer(document){
    return new Promise((resolve, reject) => {
        setTimeout(()=> {
            serverDocuments.push(document);

            const error = document.name.length > 0 ? false : true;

            if(!error){
                resolve(10);
            } else {
                reject("Problem adding document on server!");
            }
        }, 1000);
    });
}
    
// The Async/Await Solution
async function documentsHandling(){
    try{
        await addDocumentOnServer({id: 999, name: "New new new Document", size: 0.92});
        getDocuments();
    } catch(err){
        console.log(err);
    }
}
documentsHandling();
```