# **Beyond JavaScript The Journey**
Beyond JavaScript is a set of knowledge sharing sessions that are designed to cover most of the unique JavaScript features that might be overlooked or misunderstood. It is a great way to get a better understanding of how the language works or get extra information on features that are already familiar to developers. These knowledge sharing sessions are amazing for developers that already use JavaScript, but there is a refresher course segment for developers that are not very familiar with the language as well so that they can easily onboard and get value from the rest of the sessions.  
## Structure of sessions
**0.** [Refresher](JSRefresher.md) 

**1.** [Under the hood](Session%201)

**2.** [Functions](Session%202)

**3.** [Asyncrhonus programming](Session%203)

**4.** [Object Oriented programming](Session%204)

**5.** [Handling Events](Session%205)

**6.** Bonus - Patterns and good practices