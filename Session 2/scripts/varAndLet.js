// Function with var
function add5(number){
    if(number != 0){
        var five = 5;
    }
    console.log(number + five);
}
// What JavaScript actually does
function add5(number){
    var five;
    if(number != 0){
        five = 5;
    }
    console.log(number + five);
}

// Function with let
function add5(number){
    if(number != 0){
        let five = 5;
    }
    console.log(number + five);
}

// Problem with var and loops
for(var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(`Value of i : ${i}`); 
    },500);
 } 

 // Problem with var solved
 for(let i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(`Value of i : ${i}`); 
    },500);
 } 

 // Hoisting
console.log(x);
var x = 4;

var x;
console.log(x);
x = 4;

// Not with let
console.log(x);
let x = 4;

// hoisted function
``