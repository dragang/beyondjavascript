// Simple function
function sayHello(name){
    return `Hello ${name}!`;
}

// Simple arrow function ( no block )
sayHello = name => `Hello ${name}`;

// No parameters arrow function
let sayHelloNick = () => "Hello Nick!";

// With multiple parameters
sayHello = (firstName, lastName) => `Hello ${firstName} ${lastName}!`;

// Statement
sayHello = (name, isLoud) => {
    if(isLoud){
        return `HELLO ${name.toUpperCase()}`;
    } else {
        return `Hello ${name}`;
    }
}


