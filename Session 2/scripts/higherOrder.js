const peopleArr = [
    { firstName: "Dragomir", lastName: "Dragomishevski", age: 20, employed : true },
    { firstName: "Ole", lastName: "",age: 19, employed : true },
    { firstName: "Anglo", lastName: "Saksonski", age: 14, employed : false },
    { firstName: "Angele", lastName: "Odrabota", age: 34, employed : true },
    { firstName: "Silve", lastName: "Sterstalone", age: 17, employed : false },
    { firstName: "Dzontra", lastName: "Volta", age: 50, employed : false },
    { firstName: "Rambo", lastName: "Dva", age: 75, employed : true }
   ];

// Get all people 18 and above
let above18 = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].age >= 18) {
        above18.push(peopleArr[i]);
    }
  }

// Get all people 18 and above HIGHER ORDER FUNCTION
above18 = peopleArr.filter(person => person.age >= 18);

// Add full names of unemployed people
let unemployedFullNames = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].employed == false) {
        unemployedFullNames.push(`${peopleArr[i].firstName} ${peopleArr[i].lastName}`); 
    }
  }

// Add full names of unemployed people HIGHER ORDER FUNCTION
unemployedFullNames = peopleArr.filter(person => !person.employed)
.map(person => `${person.firstName} ${person.lastName}`);

// Get colective age of people and above 18
let collectiveAge = 0;
above18 = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].age >= 18) {
        above18.push(peopleArr[i]);
        collectiveAge += peopleArr[i].age;
    }
  }

// Get colective age of people above 18 HIGHER ORDER FUNCTION
collectiveAge = peopleArr.filter(person => person.age >= 18)
                .reduce((sum, person) => sum + person.age, 0);

const isAbove18 = person => person.age >= 18;
const personAge = person => person.age;
const sum = (sum, number) => sum + number;
// Using stored functions as an attribute functions
collectiveAge = peopleArr.filter(isAbove18)
                .map(personAge)
                .reduce(sum);