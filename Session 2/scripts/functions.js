// A function declaration
function sayHello(name){
    let result = `Hello there ${name}`;
    return result;
}

// A function declaration but in a variable
let sayGoodbye = function(name){ 
    let result = `Goodbye there ${name}`;
    return result;
}

// A function that gives a random name
function giveRandomName(){
    let names = ["Bobi", "Petre", "Kire", "Marko", "Fajdiga"];
    let randomNumber = Math.floor(Math.random() * (names.length));
    return names[randomNumber];
}

// A function declaration using other functions as parameters
function saySomething(sayingFunction, name){
    let result = typeof(name) == "function" ? 
    sayingFunction(name()) : sayingFunction(name);
    return result;
}

// Hold functions in an array
let myFunctions = [sayGoodbye, sayHello, saySomething, giveRandomName];
let sayFunctions = [sayGoodbye, sayHello];

// Calling a function from variable
sayGoodbye("Bob");
// Calling a function from an array
myFunctions[1]("Greg");
// Calling all functions from an array
for(let i = 0; i < sayFunctions.length; i++){ 
    console.log(sayFunctions[i]("Greg")) 
};

// Setting property to a function
sayHello.description = "A function that says hello!";
// Delete property
delete sayHello.description
// Setting property to a function (Array)
sayHello.comments = [];
// Adding an item in function array property
sayHello.comments.push("The function works great!");
// Adding a method to a function
sayHello.sayRandomName = function(randomFunct){ return `Hello there ${randomFunct()}`};




