# JS Functions
Functions are the javascript backbone and we should know how they work and what can we do with them. The main reason for using a function is that we can encapsulate code inside of it and use it as a grouped code later anywhere and any amount of times we want. This gives us the benefit of organizing our code better, grouping code by business logic, reuse code without rewriting it as many times as we want as well as making sure that the code we run is always the same and when it changes it reflects on the whole application where it is used. A unique thing about JavaScript is that functions are also first-class citizens. A first-class citizen in a language is an entity that supports all operations that other entities have, hence the name first-class citizen. This means that we can do almost anything with our functions in javascript, and that is:
* Be named by variables
* Be passed as arguments to other functions
* Be returned as the results of other functions
* Be included in data structures such as objects and collections
## Function structure
Similar to most functions in other languages, JavaScript has:
* Name - Identifier with we can call the function later in our code
* Parameters - Placeholders for values that need to be filled in by the caller of the function
* Return - Value that is returned as a result of calling a function. If there is no return, JavaScript functions return undefined

The only difference is that, since it is a loosely typed language, we don't write any data types for the return value or the parameters. There are also no access modifiers or static and dynamic functions. 
```csharp
public static string SayHello(string name)
{
	string resut = $"Hello {name}";
	return result;
}
```
```js
function SayHello(name){
	let result = `Hello ${name}`;
	return result;
}
```
#### First Class Citizen 
```js
// Standard function declaration
function sayHello(name){
    let result = `Hello there ${name}`;
    return result;
}
console.log(sayHello("Bill"));
// Declaration stored in a variable
let sayGoodbye = function(name){ 
    let result = `Goodbye there ${name}`;
    return result;
}
console.log(sayGoodbye("Bill")); // We call the variable as a function. Since we added a function to the variable, the variable now acts as function
// Functions sent as parameters to another function
function giveRandomName(){
    let names = ["Bobi", "Petre", "Kire", "Marko", "Fajdiga"];
    let randomNumber = Math.floor(Math.random() * (names.length));
    return names[randomNumber];
}
function saySomething(sayingFunction, name){
    let result = typeof(name) == "function" ? 
    sayingFunction(name()) : sayingFunction(name); // We call the sayingFunction parameter as if it was a function. The function will be provided by the caller
    return result;
}
console.log(saySomething(sayGoodbye, giveRandomName)); // When providing functions, the () brackets are left out. If we use brackets, we are not providing a function, but calling one
// Keeping functions in an array
let myFunctions = [sayGoodbye, sayHello, saySomething, giveRandomName]; // We store the functions without the () brackets. If we add the brackets, we would execute the functions and store the results of the functions, instead of the whole functions
let sayFunctions = [sayGoodbye, sayHello];
for(let i = 0; i < sayFunctions.length; i++){ 
    console.log(sayFunctions[i]("Ted")) 
};
// Treating a function as an object
// Setting property to a function
sayHello.description = "A function that says hello!";
// Delete property
delete sayHello.description
// Setting property to a function (Array)
sayHello.comments = [];
// Adding an item in function array property
sayHello.comments.push("The function works great!");
// Adding a method to a function
sayHello.sayRandomName = function(randomFunct){ return `Hello there ${randomFunct()}`};
```
## Scope
Now, there is a feature in javascript that goes hand-to-hand with functions and that is the javascript scope. The scope is very important because we use scope all the time. Every javascript code ever was written in some scope. So what is it? Well, scopes are areas where our code lives. Depending on the scope, we know where we can access stuff and where we can’t. It’s like an area with certain rules that dictate where we can use things. Functions can be declared inside of other functions in javascript. With this, we create scopes within the scope and that is called scope chain. 
![](image)
| Line | What happens               | Scope  |
|------|----------------------------|--------|
| 1    | globalNumber is declared   | Global |
| 2    | sumPlusOne is called       | Global |
| 3    | num1 and num2 are assigned | Global |
| 4    | one is declared            | A      |
| 5    | console.log is called      | A      |
| 6    | add5 is called             | A      |
| 7    | number is assigned         | A      |
| 8    | console.log is called      | B      |
| 9    | add10 is called            | A      |
| 10   | number is assigned         | A      |
| 11   | console.log is called      | C      |
So the example above visualizes scopes. These scopes are called function scopes. Those scopes are only created when a function or class is being created. But with the new EcmaScript6, there is a new type of scope. Something that the JS community has been waiting for, for quite some time and that is the block scope. Block scope is the scope between two brackets or a block of code. We can have block scope inside of functions without creating another function. As an example block scope is the for loop. The for loop creates a block scope. So now, there are two types of scopes. But we don't choose which one to use. They both exist at the same time. Like we said scopes are created as we type our code. Our interaction with the scopes is the thing that is changing. So the scopes are there but it’s up to us to choose how to use them. 
```js
function isTen(number){
	if(number == 10){
		var varResult = "It's TEN!";
		let letResult = "It's TEN!";
	}
	return varResult; // will return "It's TEN!" because the variable is contained in the function scope which are the { } of the isTen function
	return letResult; // will throw ReferenceError: letResult is not defined because the letResult variable is contained in the block scope which are the closest { } and those are the ones of the if statement
}
// These will both throw ReferenceError since we can't access the var since it is outside of the function scope, and let because we are outside of the block scope
console.log(varResult);
console.log(letResult);

// Function with var
function add5(number){
    if(number != 0){
        var five = 5;
    }
    console.log(number + five);
}
// What JavaScript actually does
function add5(number){
    var five;
    if(number != 0){
        five = 5;
    }
    console.log(number + five);
}

// Function with let
function add5(number){
    if(number != 0){
        let five = 5;
    }
    console.log(number + five);
}

// Problem with var and loops ( All logs are 6 )
// Since i is declared with var, it goes at the top of the function scope. That is outside of the for loop. i gets to 6 and stops looping. After 500ms the console logs are executed 
for(var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(`Value of i : ${i}`); 
    },500);
 } 

 // Problem with var solved ( logs are 1 2 3 4 5 )
 for(let i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(`Value of i : ${i}`); 
    },500);
 } 
```

## Hoisting
All variables that are declared with var go at the top of the document and that is because of hoisting. Javascript has this strange feature called hoisting, where it gets all declarations and it puts them on the top of the scope that is currently in. But this goes only for declarations and not for definition, the definition stays on the line where you wrote it. With this said, if you write var x = 5; javascript will create a declaration of x in the global scope and then when you get to your line it will just give 5 as a value. This means that if you wrote your code on line 100, you can still access the variable x and it exists in the global scope for all 99 lines before it. This can sometimes create confusion. The same happens for functions, and as we said last time we can call functions before declaring them and it will work. If this feature annoys you then ES6 has your back. The let variable type is not susceptible to hoisting, so when we declare a variable with let we are sure that that is the line where the declaration happens.

## Arrow functions
We want to keep our functions and callbacks short, so a great way to do this is to use arrow functions. These functions can be written without curly brackets if there is only one statement and we don’t even need a return. We write return and brackets only when the function has multiple statements. Since there was a change in scope in the new javascript we can see that there is also a change in scope for the functions. These new functions act differently with the keyword this. The this keyword in normal functions points to the nearest function scope, while with arrow functions it points to the object that the function might be wrapped in. This can make a huge difference if you have an object, write this and the function selects the window object instead of your object context. The main argument about these functions is that they are shorter and more readable.
```js
// Simple function
function sayHello(name){
    return `Hello ${name}!`;
}

// Simple arrow function ( no block )
sayHello = name => `Hello ${name}`;

// No parameters arrow function
let sayHelloNick = () => "Hello Nick!";

// With multiple parameters
sayHello = (firstName, lastName) => `Hello ${firstName} ${lastName}!`;

// Statement
sayHello = (name, isLoud) => {
    if(isLoud){
        return `HELLO ${name.toUpperCase()}`;
    } else {
        return `Hello ${name}`;
    }
}
```
## Higher order functions
Higher order functions are actually a pretty simple concepts. They are functions that accept other functions as arguments. That is about it. But we already knew javascript can do that, just didn’t know it had a name right? Well yes, but javascript introduced some internal high order functions in ES6 that really changed how people develop in javascript. Most popular are undoubtedly filter, map and reduce. Their combined power can basically rid us of a lot of code, make our code cleaner and simpler and also more efficient.
```js
const peopleArr = [
    { firstName: "Dragomir", lastName: "Dragomishevski", age: 20, employed : true },
    { firstName: "Ole", lastName: "",age: 19, employed : true },
    { firstName: "Anglo", lastName: "Saksonski", age: 14, employed : false },
    { firstName: "Angele", lastName: "Odrabota", age: 34, employed : true },
    { firstName: "Silve", lastName: "Sterstalone", age: 17, employed : false },
    { firstName: "Dzontra", lastName: "Volta", age: 50, employed : false },
    { firstName: "Rambo", lastName: "Dva", age: 75, employed : true }
   ];

// Get all people 18 and above
let above18 = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].age >= 18) {
        above18.push(peopleArr[i]);
    }
  }

// Get all people 18 and above HIGHER ORDER FUNCTION
above18 = peopleArr.filter(person => person.age >= 18);

// Add full names of unemployed people
let unemployedFullNames = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].employed == false) {
        unemployedFullNames.push(`${peopleArr[i].firstName} ${peopleArr[i].lastName}`); 
    }
  }

// Add full names of unemployed people HIGHER ORDER FUNCTION
unemployedFullNames = peopleArr.filter(person => !person.employed)
.map(person => `${person.firstName} ${person.lastName}`);

// Get colective age of people and above 18
let collectiveAge = 0;
above18 = [];
for (let i = 0; i < peopleArr.length; i++) {
    if (peopleArr[i].age >= 18) {
        above18.push(peopleArr[i]);
        collectiveAge += peopleArr[i].age;
    }
  }

// Get colective age of people above 18 HIGHER ORDER FUNCTION
collectiveAge = peopleArr.filter(person => person.age >= 18)
                .reduce((sum, person) => sum + person.age, 0);

const isAbove18 = person => person.age >= 18;
const personAge = person => person.age;
const sum = (sum, number) => sum + number;
// Using stored functions as an attribute functions
collectiveAge = peopleArr.filter(isAbove18)
                .map(personAge)
                .reduce(sum);
```