# JS Under the hood
Have you ever asked your self: Where is my javascript code executed? How does it work? We usually open the page and it works. We don’t compile it and build it as we do in C# and Java. But just because it works on its own, doesn’t mean we should treat it as magic and move on. There are many cases where a problem that takes a whole day to figure out, is mainly a mechanic in JavaScript that the developer didn’t know about or didn’t think about researching. There are a lot of features that are unique to JavaScript and that do make a difference. 

## How is JS compiled
An interesting fact about javascript that it is single-threaded. It works with only one thread. This means that the actions are executed subsequently one by one. It is also called an interpreted language. I say it's called because that is how it is classified in many places, but actually, modern javascript is compiled in the browser. Every big browser has its own compiler, Chrome - V8, Mozilla has spidermonkey, Edge has Chakra. So you might think this is a mistake, but the language in its early phases was interpreted and ran in the browser as the code went and an interpreter translated the code to the machine. It might be confusing but I learned something interesting while working on this presentation and that is, that the interpreted and compiled languages are not really about the languages but the way they are implemented and as time passed by and the web became more sophisticated, the browsers needed to be upgraded so they can run more efficiently JavaScript, so compilers for JavaScript were invented, which greatly improve execution speed and thus, made the JavaScript a compiled language. With that out of the way let's try and understand how the code is actually executed. Javascript is running in scripts, either inline or an external. The code is executed all the same. But it is not executed on loading the page. It is executed at the moment the script tag is encountered. So when a script tag is encountered the rendering stops and the scripts are executed. This is cool but with this in mind, we must be careful where we put our scripts. Even tho it is not important if the scripts are external or inline, it is important where we put our scripts. I think we all know that scripts go at the end of the HTML document, but maybe someone didn’t know why. So if we put a script before we load the HTML we can’t use the HTML that has not been loaded after the script. Also if we put 2 scripts and the first one has something that the other needs, then that script should be first so it runs first and sets the necessary items for the second. Javascript is not totally different from other languages, for instance, if you declare a function the code inside is not executed until the function is called. But there is a trick in reading the code in JavaScript. Variable declared with var and function declarations are actually given a priority and are defined and set at the top of the global scope. After this everything runs as expected. This gives us the advantage to call a function and after a few lines declare it. JavaScript will not see this as a problem. This is called hoisting but we will dive deeper into the subjects in later sessions. But you may be wondering, how is this language running on one thread, but we are getting all these cool web applications and features? Well, That is where the browser comes.

## How the browser works
The browser is the second part of the amazing technology that runs our web pages and applications. You may think it is obvious that the browser runs our pages but you will be surprised how much stuff it does. So for the browser to run our code it needs, the runtime environment and compiler. But it also needs a bunch of other mechanisms to work. The main place where all our JavaScript code is executed is actually the main stack. We will refer to it as just the stack in this presentation. So this stack keeps track of what functions we are currently executing and what functions need to be executed after that. When we get an error in the console it actually shows us that one stack that JavaScript has and what it had at the moment when it broke. So in theory, if we make an infinite function, the browser should freeze. And that is true. In theory, if we put a timeout of 10 seconds, the timeout will also freeze, because the function will wait 10 seconds in the stack. The same goes if we make a call to a server and wait 10 seconds for the server to respond. But in practice, this doesn’t happen. This is because the javascript runtime is not the only entity that powers a website. It’s a collaboration between the runtime and the browser. The browser keeps an inventory of APIs and builds objects that help the JS runtime better manage all the things that happen with the webpage. This includes objects like document and window and their properties and methods. That is why with document object we access methods like selectors and event listeners, and with the window object that has methods like settimeout and properties like console where we do the infamous console.log. Yes, we do these things in our Window object, they are not something that the javascript runtime does alone and magically. These APIs also host all the event listeners for our application, so they don’t block the stack. So when we add event listener it gets in the stack, executes, and goes in the browser servicing table or event table. There it waits for an event. Notice that if javascript worked alone almost every function that we use would have blocked the stack for a short time or even forever. But these places are not separate runtimes for javascript. They are just delegates, they hear for an event and delegate the function or code that needs to be run. They are not running it or even checking the code. These APIs and event tables push everything to the event queue or callback queue. I looked at many articles and a lot of people call it callback queue but the MDN documentation called it event queue and some other articles so I am going to call it an event queue. So the event queue is another stack that queues events that are ready to get into the stack when the stack is done doing its job. This also does not execute functions and code, it just keeps the code and functions until the main stack is empty and pushes them one at a time on to it and in the main stack, the functions are executed. It mainly keeps the order of the callback functions or events that need to be executed. But this is just a stack, it does not know when the main stack is finished or even if it has tasks. It does not have the power and logic to determine when to push these functions on to the main stack. That is when the event loop comes in place. The event loop is a constantly running process that checks if the stack is empty. So if it's empty it checks the event queue and if there are functions queued there it takes the first and puts it on to the main stack for execution. If there is nothing there it just loops and checks again.
## The event loop
The event loop is the mechanism that decides what task needs to be run first from the browser queues. We said that there is a callback queue but there is also a rendering queue somewhere in the browser and other queues that are not relevant right now for our JavaScript presentation. And it decides when to run something. As we said it looks at the main stack and if it's empty it pushes a thing from the event queue onto the main stack. So it sits there and checks all the time. It also decides when to render a page, or rather decides when to queue a task for rendering page. I will play a part in a video from a conference that I found. The guy nails the explanation of the event loop so I decided to play it because it is well explained. I don’t think I could explain it better. As we saw the event loop is the brains in a way of the whole operation. Every browser has different priorities on what tasks are more important and when it is optimal to render and when is not. We don’t want to render more times than the human can perceive, or a monitor can show. It would be a waste of resources. So the event loop decides all of this and the priority of the tasks. As you saw javascript tasks are more frequent than rendering tasks.

```js
// This code is infinite and will block your browser window and you could not do anything, type, click, select text. The main stack is held and the event loop never gets around to rendering a new frame on your browser
while(true);

// This code is also infinite, but it will not block your browser. That is because each interval is executed separately and between interactions, the browser can do different stuff, like render a frame
setInterval(function(){ console.log("interval")}, 0);

// This code also does not block the browser. That is because it waits in the browser, rather than waiting in the main thread. The code waits there as well. It can't be executed, but it can be stored so it can execute later on the main stack. 
setTimeout(function(){console.log("Waiting is done!")}, 1000000000);
```
## BOM, DOM, and DOM manipulation
So this is the background mechanics that run our web pages. As we mentioned before the browser also creates models that give javascript the power to manipulate with its contents. These are the BOM and the DOM. When the browser parses the HTML that we wrote it creates an object out of it so it can better manipulate it. That object-oriented representation of our whole web page is called the document object model or in javascript, we can access it as the document. The browser also has an object-oriented representation that is also created on page load. That is the browser object model or window object in the javascript. The window object is the main source of all the methods that we frequently use in javascript such as console.log or settimeout. The whole javascript space for writing code is the window so we don’t need to write window every time we write console. Log or access the document property. Inside the window is the document. The representation of our web page. There are other useful properties of the window such as history and location but we are not going to focus on them on this presentation. The document is our only way of changing and modifying our page dynamically and with javascript. It is an object and it contains functions and properties for almost any action and every element there is on the page. Most importantly, we can select elements in our page as objects, add events, create new elements, remove elements, and modify the CSS. JavaScript selectors have come a long way. The new query selectors changed the way we select things in javascript, giving us the jquery type of selecting by CSS selectors. This is why in this day and age it is very common for people to leave JQuery behind if they only need selecting and crude manipulations on the elements. Since now JavaScript has the query selectors, it is pointless to select with JQuery because it is slower. The point of jquery was to make things simpler and that it gives us a wide variety of tools for easy and faster coding. But if we are just using selectors and crude manipulation, it is faster to use just regular javascript. CSS manipulating from JavaScript is cool and it can be done but there is a thing that I must address. Since JS is compiled first and then CSS changes are being rendered, a lot of times the browser will ignore all the commands that are done to an element and skip to the last one. This is efficient but it can cause trouble when we are working with animations and transitions from javascript.

#### The browser in our code
```js
// The window object ( Browser Object Model )
// Every functionality that the browser serves is stored in the window object
// The window object is the global object so we don't have to write it
setTimeout(function(){ console.log("We are timing out!") }, 1000);

// This setTimeout is the same as the one above
window.setTimeout(function(){ window.console.log("We are timing out but with window!") }, 1000);

// document is our HTML and because document is a window property we have to call it by typing document
console.log(document);
// We can also call it from the window object since the document is in the browser window
console.log(window.document);
// Same goes for the console ( It is in the browser window and that is why we can access it from there )
window.cosnole.log("Hello!");
```

#### DOM selectors
```html
<section class="header">
  <h2 class="title" id="myTitle">A simple webpage with simple content</h2>
  <div class="value-props row" name="items">
    <div class="four columns value-prop row">
      <img class="value-img four columns" src="images/feather.svg">
      <div class="eight columns feather">Light as a feather at ~400 lines &amp; built with mobile in mind.</div>
    </div>
    <div class="four columns value-prop row">
      <img class="value-img four columns" src="images/pens.svg">
      <div class="eight columns">Styles designed to be a starting point, not a UI framework.</div>
    </div>
    <div class="four columns value-prop row">
      <img class="value-img four columns" src="images/watch.svg">
    </div>
  </div>
</section>
```
```js
// getElementById - Gets a single element by id 
document.getElementById("myTitle");
// getElementsByClassName - Gets a collection of elements by class ( Even if there is one reuslt or none at all, it always returns a collection )
document.getElementsByClassName("feather");
// getElementsByName - Gets a collection of elements by name ( Even if there is one reuslt or none at all, it always returns a collection )
document.getElementsByName("items");
// getElementsByTagName - Gets a collection of elements by HTML Tag ( Even if there is one reuslt or none at all, it always returns a collection )
document.getElementsByTagName("h1");
// querySelector - Gets a single element by a given query selector. If there are multiple, it gets the first one
document.querySelector(".feather");
// Complicated querySelector
document.querySelector(".header div[name='items'] .value-prop div");
// querySelectorAll - Gets a collection of elements by a given query selector. ( Even if there is one result or none at all, it always returns a collection )
document.querySelectorAll("h1");
```

#### Traversing through the DOM
```js
let element = document.getElementsByName("items")[0];
element.firstElementChild; // First child element of the current element
element.lastElementChild; // Last child element of the current element
element.children; // Returns a collection of elements that are child elements to the current element
element.parentElement; // Returns the parent element to the current element
```

#### Creating/Removing elements
```js
let parent = document.getElementsByName("items")[0];
// Create and append an element to the HTML
let newElement = document.createElement("p"); // Creating a new element ( it is not yet added to the HTML, just created in the script and stored in a variable )
newElement.innerText = "HELLO. I AM A NEW ELEMENT!"; // Adding text to the element
parent.appendChild(newElement); // Appending the new element to an aready existing element in the HTML

// Removing
parent.removeChild(newElement); // removing the same element from the HTML
```

#### CSS changes from JavaScript
```js
let element = document.getElementsByName("items")[0];
element.style.backgroundColor="red"; // The style object holds all CSS properties
// The browser always executes the SCRIPT first and then render the changes
// That means that if we do 10 changes to an element, the first 9 will never render, only the last one will render ( The firs 9 will not render even if you slow the browser to work super super slow )
items.style.display="none";
items.style.display="block";
items.style.display="none";
items.style.display="block";
items.style.display="none";
items.style.display="block";
items.style.display="none";
items.style.display="block"; // This is the only one that will render
```