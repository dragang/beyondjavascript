// // The window object ( Browser Object Model )
// // Every functionality that the browser serves is stored in the window object
// // The window object is the global object so we don't have to write it
// setTimeout(function(){ console.log("We are timing out!") }, 1000);

// // This setTimeout is the same as the one above
// window.setTimeout(function(){ window.console.log("We are timing out but with window!") }, 1000);

// // document because document is a window propertie we have to call it by typing document
// console.log(document);

// // Same as above
// window.console.log(window.document);

// Selectors

// console.log("getElementById")
// console.log(document.getElementById("myTitle"));
// console.log("getElementsByClassName")
// console.log(document.getElementsByClassName("feather"));
// console.log("getElementsByName")
// console.log(document.getElementsByName("items"));
// console.log("getElementsByTagName")
// console.log(document.getElementsByTagName("h1"));
// console.log("querySelector")
// console.log(document.querySelector(".feather"));
// console.log("Complicated querySelector")
// console.log(document.querySelector(".header div[name='items'] .value-prop div"));
// console.log("querySelectorAll")
// console.log(document.querySelectorAll("h1"));

// // Traversing nodes

let items = document.getElementsByName("items")[0];
// console.log(items.firstElementChild);
// console.log(items.lastElementChild);
// console.log(items.children);
// console.log(items.parentElement);
// console.log(items.ownerDocument);
// console.log(items.hasAttribute("name"));
// console.log(items.hasAttribute("id"));

// Adding and removing elements

// let newElement = document.createElement("p");
// newElement.innerText = "HELLO. I AM A NEW ELEMENT!";
// items.appendChild(newElement);

// setTimeout(function(){ 
//     items.removeChild(newElement) 
// }, 2000);

// changing CSS

// items.style.backgroundColor="red";

// // Javascript is ran in batches
// // This means that this will render only the last change made here
// items.style.display="none";
// items.style.display="block";
// items.style.display="none";
// items.style.display="block";
// items.style.display="none";
// items.style.display="block";
// items.style.display="none";
// items.style.display="block";
// items.style.display="none";
// items.style.display="block";



