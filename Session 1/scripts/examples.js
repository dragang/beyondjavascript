// Example 00

// myFunction();

// function myFunction() {
//   console.log("This is only a test!");
// }

// // Example 01.1

function printText(result){
    throw new Error("Don't panic! Everything is fine.");
    return "The result is " + result;
}

function sum(a, b){
    return printText(a + b);
}

function difference(a, b){
    return printText(a - b);
}

function calculator(a, b, operation){
    if(operation === '+') return sum(a, b);
    if(operation === '-') return difference(a, b);
    return "Error";
}

// // console.log(calculator(7,2,'-'));
// // calculator(7,2,'-');

// Example 01.2

// function printText(result){
//     return "he result is " + result;
// }
// function sum(a, b){
//     return printText(a + b);
// }

// function difference(a, b){
//     return printText(a - b);
// }

// function calculator(a, b, operation){
//     if(operation === '+') return sum(a, b);
//     if(operation === '-') return difference(a, b);
//     return "Error";
// }

// setTimeout(function(){
//     console.log(calculator(7,2,'-'));
// }, 2000);

// Example 02

// setTimeout(function() {
//     for(let i = 0; i < 100000000; i++){}
//     console.log("FIRST TIMEOUT COMING YOUR WAY");
// }, 0);
// setTimeout(function() {
//     for(let i = 0; i < 100000000; i++){}
//     console.log("SECOND TIMEOUT COMING YOUR WAY");
// }, 0);
// setTimeout(function() {
//     for(let i = 0; i < 100000000; i++){}
//     console.log("THIRD TIMEOUT COMING YOUR WAY");
// }, 0);